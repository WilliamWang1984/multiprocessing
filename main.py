# Methods and implementations courtesy of:
# https://stackoverflow.com/questions/6286235/terminate-multiple-threads-when-any-thread-completes-a-task

'''
Initialise, having input as:
[[color1, sleep_time1], [color2, sleep_time2], [color3, sleep_time3], ... ]
randomized sleep time to randomly return color results
'''

import random
import os
import cv2
from MultiProcessing import MultiProcessing

def main():
    print("An Example of Generic MultiProcessing class")
     
    randR = random.uniform(0, 1)
    randY = random.uniform(0, 1)
    randB = random.uniform(0, 1)
    randG = random.uniform(0, 1)
    inputArguments = [['Red', randR], ['Yellow', randY], ['Blue', randB], ['Green', randG]]

    mtProc = MultiProcessing()    
    print("Starting MultiProcessing...")
    mtProc.swamp_processes(inputArguments)

if __name__ == "__main__":
    main()
