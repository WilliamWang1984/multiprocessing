import multiprocessing
import cv2
import os
import time
import numpy as np
import random

class MultiProcessing:

    def __init__(self):
        self.timeout = 0.5 # Seconds


    def swamp_processes(self, processParameters):
        #nProcesses = len(processParameters)

        processPool = []
        resultsQueue = multiprocessing.Queue()

        currentResult = None
        print("Printing result before process swamping")
        print(currentResult)

        for processParameter in processParameters:
            currentProcess = multiprocessing.Process(target=self.run_process, args=(processParameter, resultsQueue))
            currentProcess.start()
            processPool.append(currentProcess)
        
        while not currentResult:
            currentResult = resultsQueue.get()

        print("Printing result after process swamping")
        print(currentResult)

        # Cleanup jobs
        for proc in processPool:
            proc.terminate()


    def run_process(self, processParameter, resultsQueue):
        # Run a parallel process
        
        print('.................................') 
        print('Color is:')
        print(processParameter[0])
        print('Sleep time is:')
        print(processParameter[1])
        print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')

        time.sleep(processParameter[1])        
        resultsQueue.put(processParameter[0])
